#include <iostream>
#include <fstream>
#include <string>
#include <sstream> 
#include <stdlib.h>

using namespace std;



int plot(float *graph[], const int dim, string outPutName){
	string graph_command = "Graph G {\n";
	fstream out;
	char origin[2];
	char destiny[2];
	char weight[5];
	for (int i = 0; i < dim; ++i)
	{
		for (int j = 0; j < dim; ++j)
		{	
			
			if (graph[i][j] > 0 && i != j)
			{	graph_command += "\t";
				sprintf(origin, "%d", i);
				graph_command += origin;//to_string(graph[i][0]);
				graph_command += " -- ";
				sprintf(destiny, "%d", j);
				graph_command +=  destiny;
				sprintf(weight, "%.4g", graph[i][j] );
				graph_command +=  "[label= \"";
				graph_command += weight;
				graph_command += "\"];	\n";
			}
		}

	}

	graph_command += "}\n";
	//cout << graph_command;
	
	out.open("data.txt");
	out << graph_command;
	return 0;
}


int main(){

	string plot_command = "dot -Tps data.txt -o output.ps";

	float **grafo = new float*[4];
	for (int i = 0; i < 4; ++i)
	{
		//cout<< " \n\n\n\n"<<i<< " \n\n\n\n";
		grafo[i] = new float[4];

		for (int j = 0; j < 4; ++j)
		{
			grafo[i][j] = i*j + 1;
			//cout<<grafo[i][j];
		}
	}
	plot(grafo, 4, "lol");
	system(plot_command.c_str());
}

