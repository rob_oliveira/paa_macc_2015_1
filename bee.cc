#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include <cctype>
#include <string>
#include <vector>
#include <queue>
#include <stack>
#include <set>
#include <map>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <utility>
#include <inttypes.h>
#include <fstream>

#include <arpa/inet.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <sys/time.h>

using namespace std;

#define _inline(f...) f() __attribute__((always_inline)); f

#define REP(i,n) for(int i = 0;i < n;++i)
#define FUP(i,a,b) for(int i = (a); i <= (b);++i)
#define FDOWN(i,a,b) for(int i = (a); i >= (b);--i)

#define MAX(x,y) ((x) > (y) ? (x) : (y))
#define MIN(x,y) ((x) < (y) ? (x) : (y))

#define ABS(x) ((x) < 0 ? -(x) : (x))

#define PB push_back
#define MP make_pair

const int INF = 0x3F3F3F3F ;
const int NULO = -1 ;
const double EPS = 1e-10 ;

_inline(int cmp)(double x, double y = 0, double tol = EPS) {
	return (x <= y + tol) ? (x + tol < y) ? -1 : 0 : 1;
}

#define NBEES 15
#define FLIP 1

typedef struct{
	int areamin;
	int areamax;
	int solution;
	int *D;
	int **localtable;
} bee;

int *dancetable;
int **D;
int cla;

int sumcla(int *sol, int n, int *cla, int n_cla, int weight){

	int ntrue = 0;
	REP(i, n_cla){
		ntrue += (cla[ i ] < 0 ? (sol[ ABS(cla[ i ]) - 1 ] + 1)%2 : sol[ cla[ i ] - 1 ]);
	}

	return ntrue ? weight : 0;
}

int *perturbsol(int *sol, int n_sol){
	int *vizinho = (int *) malloc (n_sol * sizeof(int));
	struct timeval tv;


	REP(i ,n_sol) vizinho[ i ] = sol[ i ];

	gettimeofday(&tv, NULL);
	srand(tv.tv_sec * tv.tv_usec);
	int num = rand() % n_sol;
	vizinho[ num ] = (sol[ num ] + 1)%2;

	return vizinho;
}

int cmpvet(int *a, int *b, int n){
	REP(i, n) if(a[ i ] != b[ i ]) return 1;
	return 0;
}

void ringright(int *v, int a, int n){
	int aux = v[ n ];
	for(int i = n; i > a; i--){
		v[ i ] = v[ i - 1 ];
	}
	v[ 0 ] = aux;
}

void buscalocal(bee *B, int var, int *n_D, int *W){
	//flip 1 bits -- busca em anel pela melhor configuração
	int len = B->areamax - B->areamin + 1;
	int best[ len ];
	int j = 0;

//Procura pra cada vizinho pra direita e soma as clausulas
	int localbest = -1;
	REP(kk, len){
		int tmpsol = 0;
		//Alterar a logica para convergir rapido
		ringright(B->D, B->areamin, B->areamax);
		REP(i, cla){ tmpsol += sumcla(B->D, var, D[ i ], n_D[ i ], W[ i ]); }
		if(localbest < tmpsol){
			localbest = tmpsol;
			int j = 0;
			FUP(i, B->areamin, B->areamax){
				best[ j++ ] = B->D[ i ];
			}
		}
	}
	//volta a abelha pra posição original
	ringright(B->D, B->areamin, B->areamax);

//encontrada a melhor solução local copia pra tabela de dança
	int value = B->areamin;
	REP(i, len)	dancetable[ value++ ] = best[ i ]; 
}

void designviz(bee *BEE, int n_bees, int *viz, int var){
	REP(i, n_bees){
		BEE[ i ].D = (int *) malloc (var * sizeof(int));
		REP(j, var) BEE[ i ].D[ j ] = viz[ j ];
	}
}

void plot(){
	fstream out;
	out.open("plot_file.gp");
	string plot_comand = "plot \"data.dat\" using 2:1 w l  title 'jnh220'\n pause -1";
	out << plot_comand;
	plot_comand = "gnuplot plot_file.gp";
	out.close();
	system(plot_comand.c_str());
}


int main(int argc, char**argv)
{
	unsigned int seed = 13;
	struct timeval tv;

//READING
	int var;
	scanf("%d %d", &var, &cla);

	int csol[ var ];
	int n_D[ cla ];
	int W[ cla ];

	gettimeofday(&tv, NULL);   					// marca o tempo final	
	double tini = (double)(tv.tv_sec + tv.tv_usec/1e6); 


	
	D = (int **) malloc (cla * sizeof(int *));
	REP(i, cla) D[ i ] = (int *) malloc (var * sizeof(int));

	REP(i, cla) REP(j, var) D[ i ][ j ] = -1 ;

	REP(i, cla){
		int aux;
		scanf("%d %d", &n_D[ i ], &W[ i ]);
		REP(j, n_D[ i ]){
			scanf("%d", &D[ i ][ j ]);
		}
	}

//GENERATE RANDOM SOLUTION
	gettimeofday(&tv, NULL);
	srand(tv.tv_sec * tv.tv_usec);
	REP(i, var) csol[ i ] = rand()%2;	
//
	dancetable = (int *) malloc (var * sizeof(int));
	memset(dancetable, 0, sizeof(dancetable));

//Inicializa a primeira solução e o valor dos pesos
	int bestvalue = 0;
	int bestsol[ var ];

	REP(i, cla){ bestvalue += sumcla(csol, var, D[ i ], n_D[ i ], W[ i ]); }
	REP(i, var) bestsol[ i ] = csol[ i ];
	int init_sol = bestvalue;
	int samples = 0;
	int n_bees;

	if(var < NBEES) n_bees = var;
	else n_bees = NBEES;

	bee BEE[ n_bees ];

//Inicializa as abelhas determinando a area de atuação
	REP(i, n_bees){
		int media = var / n_bees;
		BEE[ i ].areamin = media * i;
		if( i == n_bees - 1){
			BEE[ i ].areamax = var - 1; 
		}
		else
			BEE[ i ].areamax = (media * i + media) - 1;
		
		BEE[ i ].solution = bestvalue;
	}

	designviz(BEE, n_bees, csol, var);

	fstream fout("data.dat", fstream::out);
	if (!fout.is_open())	{
		cout << "Erro ao abrir arquivo de soluçoes\n";
		return -1;
	}
	
	double media = 0;
	for (int count = 0; count < 3; ++count)
	{
		
		while(samples++ < 30000){ //ainda tem que pensar na condição de parada
			//cout << " iteracao " << samples << endl;
			//gera a perturbação na solução
			int *viz;
			viz = perturbsol(csol, var);
			//Prepara a tabela de dança pras abelhas procurarem a melhor solução.
			REP(i, var) dancetable[ i ] = csol[ i ];
			//Designar o vizinho para cada abelha
			designviz(BEE, n_bees, viz, var);
			//gera a perturbação local
			REP(i, n_bees){
				buscalocal(&BEE[ i ], var, n_D, W);
			}
			//verifica o ganho na solução em cima da dance table
			int csolvalue = 0;
			REP(i, cla){ csolvalue += sumcla(dancetable, var, D[ i ], n_D[ i ], W[ i ]); }
			fout << bestvalue << " " << samples << endl;
			//Se há ganho update na solução
			if(csolvalue >= bestvalue){
				
				bestvalue = csolvalue;
				REP(i, var)	bestsol[ i ] = dancetable[ i ];
				REP(i, n_bees) REP(j, var) BEE[ i ].D[ j ] = dancetable[ j ];
				cout << "BEST: " << bestvalue << endl << "Solution: ";
				fout << bestvalue << endl;
				REP(i, var){
					//cout << " " << bestsol[ i ];
				}
				cout << endl;
			}
			REP(i, var) csol[ i ] = dancetable[ i ];
		}
		media += bestvalue;
	}
	fout.close();
	gettimeofday(&tv, NULL);   					// marca o tempo final	
	double tend = (double)(tv.tv_sec + tv.tv_usec/1e6);

	double total = tend - tini;
	//double optimun_sol = 393238;
	cout << "solucao inicial = " << init_sol << endl;
	cout << "solucao final   = " << bestvalue << endl;
	cout << "tempo total     = " << total << endl;
	cout << "media           = " << media/3;
	cout << "Melhor solucao   = ";
	REP(i, var){
		cout << " " << bestsol[ i ];
	}
	cout << endl;
	plot();
	return 0;
}


